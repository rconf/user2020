# Technological Setup of Why R? Webinars

[Based on this](https://it.umn.edu/zoom-live-stream-youtube-or-custom)

In terms of ingredients - you would need to have

1. YouTube channel - [like this one](https://www.youtube.com/channel/UCwLy_PYrnCEhCU-Ay2F5Drw)

    1. You might want to [customize your youtube url](https://support.google.com/youtube/answer/2657968?visit_id=637204912253455027-2698202457&p=custom_URL&hl=en&rd=1) - this requires 100 subscribers

2. Zoom account

    2. At least 1 host on Pro license ($14.90 a month) - [pricing](https://zoom.us/pricing)

Setup

1. Schedule a YouTube streaming

    1. Go to your channel’s studio [https://studio.youtube.com/](https://studio.youtube.com/)

    2. Hit Create -> Go Live (top right panel)

    3. Setup title of the stream, category and the scheduled data
![image alt text](image_0.png)

    4. Example below
![image alt text](image_1.png)

    5. Setup of keys - you will need to use **Stream key** to connect with Zoom
![image alt text](image_2.png)

2. Setup up Zoom meeting

    6. Go to web panel - [https://zoom.us/meeting](https://zoom.us/meeting)

    7. The rest is based on [Setting up Custom Live Streaming (Host)](https://support.zoom.us/hc/en-us/articles/115001777826-Live-Streaming-Meetings-or-Webinars-Using-a-Custom-Service#h_62b792dc-3cf9-4b62-848d-93ee9e412a7c)

    8. That’s the example fields you put in the last part
![image alt text](image_3.png)
Where Live streaming page URL can be obtained when you select **Share** in YouTube Studio
![image alt text](image_4.png)

### Example

That is the screen people see when they join YouTube url and the meeting is not yet started
![image alt text](image_5.png)

When you are ready with your Zoom meeting, ask the speaker to share the screen and then click **More -> Live on Custom Live Streaming Service**
![image alt text](image_6.png)

You will see a preview on YouTube studio. When you are ready hit Go Live on top right panel in YouTube studio - remember there is a 15 seconds delay!

