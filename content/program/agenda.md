---
title: Agenda
output: 
  md_document:
    preserve_yaml: true
---

useR! 2020 is now a virtual conference.
---------------------------------------

The conference will be free and online. Talks will be available on
[YouTube](https://www.youtube.com/channel/UC_R5smHVXRYGhZYDJsnXTwg)
either live or pre-recorded. All YouTube content will be captioned.

There is no need to sign up or register for the talks. Just come by!

A calendar of events can be found at the bottom of the page. You can
also subscribe to the calendars via
[iCal](https://calendar.google.com/calendar/ical/a38nnigbugd5bebvvdvsi60jbo%40group.calendar.google.com/public/basic.ics)
or
[Google](https://calendar.google.com/calendar/embed?src=a38nnigbugd5bebvvdvsi60jbo%40group.calendar.google.com&ctz=Europe%2FBerlin)

Questions for all keynote talks are being hosted on
[slido](https://app.sli.do/event/ccilmxcz/live/questions).

Live captions for all keynotes can be found at
[streamtext](https://www.streamtext.net/player?event=useR2020).

### Breakout Sessions

<table class="table" style="margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Date
</th>
<th style="text-align:left;">
Time
</th>
<th style="text-align:left;">
Duration
</th>
<th style="text-align:left;">
Speakers
</th>
<th style="text-align:left;">
Title (with video link)
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;width: 20%; ">
Tuesday, July 07, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-07-07/14:00/Tackling%20Climate%20Change">14:00
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
02:00
</td>
<td style="text-align:left;width: 30%; ">
Olga Mierzwa-Sulima, Tadeusz Bara-Slupski
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://lmu-munich.zoom.us/j/97384976868?pwd=aWFkdDVFVW1JYjMxNytnSXM0N1h1dz09">Tackling
Climate Change</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 20%; ">
Friday, July 17, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-07-17/14:30/Supporting%20diversity%20in%20the%20R%20community">14:30
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
01:00
</td>
<td style="text-align:left;width: 30%; ">
Yanina Bellini Saibene, Lais Carvalho, Richard Ngamita, Danielle
Smalls-Perkins, Robin Williams, Greg Wilson
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://youtu.be/gDO1OphmF5Q">Supporting diversity in the R
community</a>
</td>
</tr>
</tbody>
</table>

### Welcome Session

<table class="table" style="margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Date
</th>
<th style="text-align:left;">
Time
</th>
<th style="text-align:left;">
Duration
</th>
<th style="text-align:left;">
Speakers
</th>
<th style="text-align:left;">
Title (with video link)
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;width: 20%; ">
Wednesday, July 08, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-07-08/16:00/nR-ROBERT%3A">16:00
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
00:15
</td>
<td style="text-align:left;width: 30%; ">
Chris, Jenine, Heidi
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://youtu.be/X_eDHNVceCU">nR-ROBERT:</a>
</td>
</tr>
</tbody>
</table>

### Keynote Sessions

<table class="table" style="margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Date
</th>
<th style="text-align:left;">
Time
</th>
<th style="text-align:left;">
Duration
</th>
<th style="text-align:left;">
Speakers
</th>
<th style="text-align:left;">
Title (with video link)
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;width: 20%; ">
Wednesday, July 08, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-07-08/16:15/New%20Developments%20in%20R%204.0.0%20and%20Beyond%20%28Part%201%20and%20Part%202%29">16:15
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
01:30
</td>
<td style="text-align:left;width: 30%; ">
Martin Mächler + Luke Tierney
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://youtu.be/X_eDHNVceCU">New Developments in R 4.0.0 and
Beyond (Part 1 and Part 2)</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 20%; ">
Thursday, July 09, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-07-09/13:00/Peer-Reviewing%20R%20Code%3A%20Where%20We%27ve%20Been%20and%20Where%20We%27re%20Going%20at%20rOpenSci">13:00
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
01:00
</td>
<td style="text-align:left;width: 30%; ">
Noam Ross
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://youtu.be/iJnn_9xKkqk">Peer-Reviewing R Code: Where
We’ve Been and Where We’re Going at rOpenSci</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 20%; ">
Thursday, July 09, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-07-09/18:00/Speaking%20R">18:00
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
01:00
</td>
<td style="text-align:left;width: 30%; ">
Amelia McNamara
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://youtu.be/ckW9sSdIVAc">Speaking R</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 20%; ">
Friday, July 10, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-07-10/14:00/Computational%20Reproducibility%2C%20from%20theory%20to%20practice">14:00
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
01:00
</td>
<td style="text-align:left;width: 30%; ">
Anna Krystalli
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://youtu.be/KHMW8fV2NXo">Computational Reproducibility,
from theory to practice</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 20%; ">
Friday, July 10, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-07-10/16:00/Responsible%20Automation%3A%20Towards%20Interpretable%20%26%20Fair%20AutoML">16:00
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
01:00
</td>
<td style="text-align:left;width: 30%; ">
Erin LeDell
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://youtu.be/9QnmQZeslXg">Responsible Automation: Towards
Interpretable & Fair AutoML</a>
</td>
</tr>
<tr>
<td style="text-align:left;width: 20%; ">
Saturday, July 11, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-07-11/14:00/Talk%20with%20your%20model%21%20Towards%20the%20language%20for%20exploration%20and%20explanation%20of%20machine%20learning%20models.">14:00
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
01:00
</td>
<td style="text-align:left;width: 30%; ">
Przemyslaw Biecek
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://youtu.be/9WWn5ew8D8o">Talk with your model! Towards the
language for exploration and explanation of machine learning models.</a>
</td>
</tr>
</tbody>
</table>

### R Core Event

<table class="table" style="margin-left: auto; margin-right: auto;">
<thead>
<tr>
<th style="text-align:left;">
Date
</th>
<th style="text-align:left;">
Time
</th>
<th style="text-align:left;">
Duration
</th>
<th style="text-align:left;">
Speakers
</th>
<th style="text-align:left;">
Title (with video link)
</th>
</tr>
</thead>
<tbody>
<tr>
<td style="text-align:left;width: 20%; ">
Wednesday, July 08, 2020
</td>
<td style="text-align:left;width: 10%; ">
<a href="https://arewemeetingyet.com/UTC/2020-07-08/18:00/R%20Core%20panel">18:00
UTC</a>
</td>
<td style="text-align:left;width: 10%; ">
01:00
</td>
<td style="text-align:left;width: 30%; ">
Martyn Plummer (panel moderator), Tomas Kalibera, Michael Lawrence,
Robert Gentleman, Martin Maechler, Luke Tierney, Simon Urbanek, Paul
Murrel, Kurt Hornik
</td>
<td style="text-align:left;width: 30%; ">
<a href="https://youtu.be/X_eDHNVceCU">R Core panel</a>
</td>
</tr>
</tbody>
</table>
<iframe src="https://calendar.google.com/calendar/embed?src=a38nnigbugd5bebvvdvsi60jbo%40group.calendar.google.com&amp;ctz=Europe%2FBerlin" style="border: 0" width="800" height="600" frameborder="0" scrolling="no">
</iframe>
