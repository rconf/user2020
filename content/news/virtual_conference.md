+++
title = "Plans for Online useR! 2020"
date = "2020-06-18T10:40:00"
tags = ["Keynotes", "Speakers"]
categories = ["Keynotes"]
banner = "news/vr_thumbnail.png"
+++
We are excited to share our plans for a useR! 2020 series of events this July to replace the cancelled useR! 2020 Conference (St. Louis) and the useR! 2020 European Hub (Munich). In brief, our plans are to:  

1. Host virtual keynotes with Q&A in July, largely around the time useR! 2020 would have been held
2. Hold tutorials in partnership with R-Ladies Global, MiR, and the AfricaR user group
3. Curate a video collection from accepted presenters on YouTube

Some things that are part of the typical useR! experience, and make it special, will not be included in our virtual events. While the loss of social events, the gala dinner, and informal networking events will be missed, we are looking forward to their return at useR! 2021 in Zurich.

## Keynotes

Many of the keynotes we scheduled for both the St. Louis and Munich sites have agreed to give virtual presentations.. Keynotes will be live streamed via YouTube and we will use Slido to host virtual Q and A sessions, so that we can still have participation from the R community.  

*   2020-07-08 - Martin Mächler, Luke Tierney, and the R-Core Team
*   2020-07-09 - Noam Ross
*   2020-07-09 - Amelia McNamara
*   2020-07-10 - Anna Krystalli
*   2020-07-10 - Erin LeDell
*   2020-07-11 - Przemyslaw Biecek

Times will be shared soon!

## Tutorials

Tutorials will be organized in collaboration with R-Ladies, MiR, and AfricaR chapters. Chapters applied to host one of the tutorials. With this we hope to provide visibility for the respective groups, and a schedule of useR! 2020 tutorials will be posted soon along with instructions for requesting a spot.

## Presentations

We are currently soliciting short videos from R community members whose abstracts were accepted for the St. Louis and Munich conferences. Once we have collected them, we will publish them on YouTube as a collection.
